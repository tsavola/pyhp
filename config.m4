PHP_ARG_WITH(python, Python 2.6,
[  --with-python[=PREFIX]])

if test "$PHP_PYTHON" != "no"; then
  PYTHON_PREFIX=$PHP_PYTHON
else
  PYTHON_PREFIX=`python2.6 --prefix`
fi

AC_MSG_RESULT([$PYTHON_PREFIX])

PHP_ADD_INCLUDE($PYTHON_PREFIX/include/python2.6)
PHP_ADD_LIBRARY_WITH_PATH(python2.6, $PYTHON_PREFIX/lib, PYHP_SHARED_LIBADD)

PHP_NEW_EXTENSION(pyhp, pyhp.c, $ext_shared)
PHP_SUBST(PYHP_SHARED_LIBADD)
