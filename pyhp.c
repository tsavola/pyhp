#include <Python.h>

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include "php.h"
#include "zend_exceptions.h"

#ifdef ZTS
# include "TSRM.h"
#endif

#define P_WRAPPER_NAME   "PythonWrapper"
#define P_EXCEPTION_NAME "PythonException"
#define P_RESOURCE_NAME  "Python Object"
#define P_PROPERTY_NAME  "_python_object"

static zend_class_entry *p_wrapper;
static zend_class_entry *p_exception;
static int p_resource;

static bool p_is_php_array_associative(HashTable * TSRMLS_DC);
static PyObject *p_make_python_list(HashTable * TSRMLS_DC);
static PyObject *p_make_python_dict(HashTable * TSRMLS_DC);

/**
 * Throw a PHP exception.
 *
 * @param message is borrowed.
 * @throw PythonException for PHP.
 */
static void p_throw_exception(char *message TSRMLS_DC)
{
	zval *exception;

	MAKE_STD_ZVAL(exception);
	object_init_ex(exception, p_exception);

	if (message)
		zend_update_property_string(p_exception, exception, "message", strlen("message"), message TSRMLS_CC);

	zend_throw_exception_object(exception TSRMLS_CC);
}

/**
 * Clear Python error state and throw a PHP exception.
 *
 * @throw PythonException for PHP.
 */
static void p_throw_python_exception(TSRMLS_D)
{
	PyObject *type;
	PyObject *value;
	PyObject *trace;
	PyObject *string;
	char *details;
	char *message;

	PyErr_Fetch(&type, &value, &trace);

	string = PyObject_Str(value);
	if (string)
		details = PyString_AsString(string);
	else
		details = "";

	PyErr_Clear();

	spprintf(&message, 0, "%s: %s", ((PyTypeObject *) type)->tp_name, details);
	p_throw_exception(message TSRMLS_CC);

	Py_XDECREF(string);
	Py_DECREF(trace);
	Py_DECREF(value);
	Py_DECREF(type);
}

/**
 * Create a PHP resource which references the Python object and a PHP object
 * which contains the PHP resource.
 *
 * @param object_php is a pre-allocated zvalue which will hold a new
 *        PythonWrapper instance.
 * @param object_py is an arbitrary object.
 */
static void p_make_php_object(zval *object_php, PyObject *object_py TSRMLS_DC)
{
	zval *resource;

	MAKE_STD_ZVAL(resource);
	ZEND_REGISTER_RESOURCE(resource, object_py, p_resource);

	object_init_ex(object_php, p_wrapper);
	zend_update_property(p_wrapper, object_php, P_PROPERTY_NAME, strlen(P_PROPERTY_NAME),resource TSRMLS_CC);
}

/**
 * Unreferences the Python object referenced by a PHP resource.
 */
static void p_destroy_resource(zend_rsrc_list_entry *rsrc TSRMLS_DC)
{
	PyObject *object = rsrc->ptr;

	Py_DECREF(object);
}

/**
 * Get the Python object referenced by the PHP resource contained in a PHP
 * object.
 *
 * @param object_php is a PythonWrapper instance.
 * @return a borrowed reference to the wrapped object.
 */
static PyObject *p_get_python_object(zval *object_php TSRMLS_DC)
{
	zval *resource;
	PyObject *object_py;

	resource = zend_read_property(p_wrapper, object_php, P_PROPERTY_NAME, strlen(P_PROPERTY_NAME), false);
	if (resource == NULL)
		php_error_docref(NULL TSRMLS_CC, E_ERROR, "Python object property not found");

	object_py = zend_fetch_resource(&resource TSRMLS_CC, -1, P_RESOURCE_NAME, NULL, 1, p_resource);
	if (object_py == NULL)
		php_error_docref(NULL TSRMLS_CC, E_ERROR, "Python object resource not found");

	return object_py;
}

/**
 * Convert a Python object to a PHP value.  None, bool, int, float, str and
 * tuple types are converted by value; other objects are wrapped.  A tuple is
 * converted recursively to an array.
 *
 * @param value_php is a pre-allocated zvalue which will hold the result.
 * @param value_py is a reference to an object which will be stolen.
 * @return SUCCESS of FAILURE.
 * @throw PythonException for PHP on FAILURE return.
 */
static int p_make_php_value(zval *value_php, PyObject *value_py TSRMLS_DC)
{
	int ret = SUCCESS;

	if (value_py == Py_None) {
		ZVAL_NULL(value_php);

	} else if (PyBool_Check(value_py)) {
		ZVAL_BOOL(value_php, PyInt_AS_LONG(value_py));

	} else if (PyInt_Check(value_py)) {
		ZVAL_LONG(value_php, PyInt_AS_LONG(value_py));

	} else if (PyFloat_Check(value_py)) {
		ZVAL_DOUBLE(value_php, PyFloat_AS_DOUBLE(value_py));

	} else if (PyString_Check(value_py)) {
		char *buffer;
		Py_ssize_t length;

		if (PyString_AsStringAndSize(value_py, &buffer, &length) >= 0) {
			ZVAL_STRINGL(value_php, buffer, length, true);
		} else {
			p_throw_python_exception(TSRMLS_C);
			ret = FAILURE;
		}

	} else if (PyTuple_Check(value_py)) {
		int argc;
		int i;

		array_init(value_php);

		argc = PyTuple_GET_SIZE(value_py);
		for (i = 0; i < argc; i++) {
			zval *item;

			MAKE_STD_ZVAL(item);
			ret = p_make_php_value(item, PyTuple_GET_ITEM(value_py, i));
			if (ret == SUCCESS) {
				zend_hash_next_index_insert(Z_ARRVAL_P(value_php), &item, sizeof (item), NULL);
			} else {
				FREE_ZVAL(item);
				break;
			}
		}

		if (ret == FAILURE) {
			zval_dtor(value_php);
			ZVAL_NULL(value_php);
		}

	} else {
		p_make_php_object(value_php, value_py TSRMLS_CC);
		value_py = NULL;
	}

	Py_XDECREF(value_py);

	return ret;
}

/**
 * Convert and insert a Python key-value pair to an associative PHP array.
 * None, int-derived and str-derived types are supported for the key.  A PHP
 * exception is generated if an unsupported key is given.  The value is
 * converted using the rules documented in p_make_php_value().
 *
 * @param ht is the array being populated.
 * @param key_py is a reference to an object which will be stolen.
 * @param value_py is a reference to an object which will be stolen.
 * @return SUCCESS of FAILURE.
 * @throw PythonException for PHP on FAILURE return.
 */
static int p_make_php_mapping(HashTable *ht, PyObject *key_py, PyObject *value_py TSRMLS_DC)
{
	zval *value_php;

	MAKE_STD_ZVAL(value_php);
	if (p_make_php_value(value_php, value_py) == FAILURE)
		goto error;

	if (key_py == Py_None) {
		zend_hash_add(ht, "", 1, &value_php, sizeof (value_php), NULL);

	} else if (PyInt_Check(key_py)) {
		zend_hash_index_update(ht, PyInt_AS_LONG(key_py), &value_php, sizeof (value_php), NULL);

	} else if (PyString_Check(key_py)) {
		char *buffer;
		Py_ssize_t length;

		if (PyString_AsStringAndSize(key_py, &buffer, &length) < 0) {
			p_throw_python_exception(TSRMLS_C);
			goto error;
		}

		zend_hash_add(ht, buffer, length + 1, &value_php, sizeof (value_php), NULL);

	} else {
		p_throw_exception("Python type not usable as PHP array key" TSRMLS_CC);
		goto error;
	}

	return SUCCESS;

error:
	zval_dtor(value_php);
	FREE_ZVAL(value_php);

	return FAILURE;
}

/**
 * Convert a PHP value to a Python object by value.  Null, bool, long, double,
 * string and array values and PythonWrapper objects are supported.  Array is
 * converted to a list or a dict depending on the keys.  A PHP exception is
 * generated if an unsupported value is given.
 *
 * @param value_php is borrowed.
 * @return a new reference, or NULL on error.
 * @throw PythonException for PHP on NULL return.
 */
static PyObject *p_make_python_value(zval *value_php TSRMLS_DC)
{
	PyObject *value_py = NULL;

	switch (Z_TYPE_P(value_php)) {
	case IS_NULL:
		Py_RETURN_NONE;

	case IS_BOOL:
		value_py = PyBool_FromLong(Z_LVAL_P(value_php));
		break;

	case IS_LONG:
		value_py = PyInt_FromLong(Z_LVAL_P(value_php));
		break;

	case IS_DOUBLE:
		value_py = PyFloat_FromDouble(Z_DVAL_P(value_php));
		break;

	case IS_STRING:
		value_py = PyString_FromStringAndSize(value_php->value.str.val, value_php->value.str.len);
		break;

	case IS_ARRAY:
		if (p_is_php_array_associative(Z_ARRVAL_P(value_php) TSRMLS_CC)) {
			value_py = p_make_python_dict(Z_ARRVAL_P(value_php) TSRMLS_CC);
		} else {
			value_py = p_make_python_list(Z_ARRVAL_P(value_php) TSRMLS_CC);
		}
		break;

	case IS_OBJECT:
		if (Z_OBJCE_P(value_php) == p_wrapper) {
			value_py = p_get_python_object(value_php);
			Py_XINCREF(value_py);
		} else {
			p_throw_exception("PHP object type not supported" TSRMLS_CC);
		}
		break;

	default:
		p_throw_exception("PHP type not supported" TSRMLS_CC);
		break;
	}

	if (PyErr_Occurred())
		p_throw_python_exception(TSRMLS_C);

	return value_py;
}

/**
 * Check if a PHP array is a map or a list.
 *
 * @param ht is a borrowed array.
 * @return false if keys were contiguous indexes.
 */
static bool p_is_php_array_associative(HashTable *ht TSRMLS_DC)
{
	HashPosition pos;
	unsigned long i;

	if (zend_hash_num_elements(ht) == 0)
		return false;

	zend_hash_internal_pointer_reset_ex(ht, &pos);

	for (i = 0; true; i++) {
		char *key;
		int key_len;
		unsigned long index;

		switch (zend_hash_get_current_key_ex(ht, &key, &key_len, &index, false, &pos)) {
		case HASH_KEY_NON_EXISTANT:
			return false;

		case HASH_KEY_IS_STRING:
			return true;

		default:
			if (index != i)
				return true;
		}

		zend_hash_move_forward_ex(ht, &pos);
	}
}

/**
 * Convert a PHP array to a Python container.  The elements will be converted
 * using the rules documented in p_make_python_value().
 *
 * @param ht is a borrowed array.
 * @return a new reference, or NULL on error.
 * @throw PythonException for PHP on NULL return.
 */
static PyObject *p_make_python_container(HashTable *ht, PyObject *(*create)(Py_ssize_t),
                                         int (*insert)(PyObject *, Py_ssize_t, PyObject *) TSRMLS_DC)
{
	int size;
	PyObject *container = NULL;
	int i;

	size = zend_hash_num_elements(ht);

	container = create(size);
	if (container == NULL) {
		p_throw_python_exception(TSRMLS_C);
		goto error;
	}

	for (i = 0; i < size; i++) {
		zval **arg_php;
		PyObject *arg_py;

		if (zend_hash_index_find(ht, i, (void **) &arg_php) == SUCCESS) {
			arg_py = p_make_python_value(*arg_php TSRMLS_CC);
			if (arg_py == NULL)
				goto error;
		} else {
			arg_py = Py_None;
			Py_INCREF(arg_py);
		}

		if (insert(container, i, arg_py) < 0) {
			Py_DECREF(arg_py);
			goto error;
		}
	}

	return container;

error:
	Py_XDECREF(container);
	return NULL;
}

static PyObject *p_py_set_create(Py_ssize_t size) { return PySet_New(NULL); }
static PyObject *p_py_frozenset_create(Py_ssize_t size) { return PyFrozenSet_New(NULL); }
static int p_py_set_insert(PyObject *set, Py_ssize_t i, PyObject *item) { return PySet_Add(set, item); }

static PyObject *p_make_python_tuple(HashTable *ht TSRMLS_DC)
{
	return p_make_python_container(ht, PyTuple_New, PyTuple_SetItem TSRMLS_CC);
}

static PyObject *p_make_python_list(HashTable *ht TSRMLS_DC)
{
	return p_make_python_container(ht, PyList_New, PyList_SetItem TSRMLS_CC);
}

static PyObject *p_make_python_frozenset(HashTable *ht TSRMLS_DC)
{
	return p_make_python_container(ht, p_py_frozenset_create, p_py_set_insert TSRMLS_CC);
}

static PyObject *p_make_python_set(HashTable *ht TSRMLS_DC)
{
	return p_make_python_container(ht, p_py_set_create, p_py_set_insert TSRMLS_CC);
}

/**
 * Convert a PHP array to a Python dictionary.  The values are converted using
 * the rules documented in p_make_python_value().
 *
 * @param ht is a borrowed array.
 * @return a new reference, or NULL on error.
 * @throw PythonException for PHP on NULL return.
 */
static PyObject *p_make_python_dict(HashTable *ht TSRMLS_DC)
{
	PyObject *dict;
	PyObject *key_py;
	PyObject *value_py;
	HashPosition pos;

	dict = PyDict_New();
	if (dict == NULL) {
		p_throw_python_exception(TSRMLS_C);
		return NULL;
	}

	if (zend_hash_num_elements(ht) == 0)
		return dict;

	zend_hash_internal_pointer_reset_ex(ht, &pos);

	while (true) {
		char *str;
		int str_len;
		unsigned long index;
		zval **value_php;

		key_py = NULL;
		value_py = NULL;

		switch (zend_hash_get_current_key_ex(ht, &str, &str_len, &index, false, &pos)) {
		case HASH_KEY_NON_EXISTANT:
			return dict;

		case HASH_KEY_IS_STRING:
			key_py = PyString_FromStringAndSize(str, str_len - 1);
			break;

		default:
			key_py = PyInt_FromLong(index);
			break;
		}

		if (key_py == NULL) {
			p_throw_python_exception(TSRMLS_C);
			break;
		}

		zend_hash_get_current_data_ex(ht, (void **) &value_php, &pos);

		value_py = p_make_python_value(*value_php);
		if (value_py == NULL)
			break;

		if (PyDict_SetItem(dict, key_py, value_py) < 0) {
			p_throw_python_exception(TSRMLS_C);
			break;
		}

		zend_hash_move_forward_ex(ht, &pos);
	}

	Py_XDECREF(value_py);
	Py_XDECREF(key_py);
	Py_DECREF(dict);
	return NULL;
}

/**
 * Call a member of a Python object.  The PHP argument vector is converted to
 * Python using the rules documented in p_make_python_value().
 *
 * @param object is a borrowed reference.
 * @param name is borrowed.
 * @param argv_php is borrowed.
 * @param retval_php is a pre-allocated zvalue for holding the converted return
 *                   value.
 * @throw PythonException for PHP on error.
 */
static void p_call_python(PyObject *object, const char *name, HashTable *argv_php, zval *retval_php TSRMLS_DC)
{
	PyObject *argv_py = NULL;
	PyObject *callable = NULL;
	PyObject *retval_py;

	argv_py = p_make_python_tuple(argv_php TSRMLS_CC);
	if (argv_py == NULL)
		goto error;

	callable = PyObject_GetAttrString(object, name);
	if (callable == NULL) {
		p_throw_python_exception(TSRMLS_C);
		goto error;
	}

	retval_py = PyObject_Call(callable, argv_py, NULL);
	if (retval_py == NULL) {
		p_throw_python_exception(TSRMLS_C);
		goto error;
	}

	p_make_php_value(retval_php, retval_py TSRMLS_CC);

error:
	Py_XDECREF(callable);
	Py_XDECREF(argv_py);
}

/*
 * PythonWrapper::__call(name, argv) -> PythonWrapper
 *
 * Throws PythonException on error.
 */
static ZEND_NAMED_FUNCTION(ZEND_MN(p_wrapper_call))
{
	char *name;
	int namelen;
	HashTable *argv;
	PyObject *self;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sh", &name, &namelen, &argv) == FAILURE)
		return;

	self = p_get_python_object(this_ptr TSRMLS_CC);
	if (self == NULL)
		return;

	p_call_python(self, name, argv, return_value TSRMLS_CC);
}

/*
 * PythonWrapper::__get(name) -> PythonWrapper
 *
 * Throws PythonException on error.
 */
static ZEND_NAMED_FUNCTION(ZEND_MN(p_wrapper_get))
{
	char *name;
	int namelen;
	PyObject *self;
	PyObject *value;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s", &name, &namelen) == FAILURE)
		return;

	self = p_get_python_object(this_ptr TSRMLS_CC);
	if (self == NULL)
		return;

	value = PyObject_GetAttrString(self, name);
	if (value == NULL) {
		p_throw_python_exception(TSRMLS_C);
		return;
	}

	p_make_php_value(return_value, value TSRMLS_CC);
}

/*
 * PythonWrapper::__isset(name) -> BOOL
 */
static ZEND_NAMED_FUNCTION(ZEND_MN(p_wrapper_isset))
{
	char *name;
	int namelen;
	PyObject *self;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s", &name, &namelen) == FAILURE)
		return;

	self = p_get_python_object(this_ptr TSRMLS_CC);
	if (self == NULL)
		return;

	if (PyObject_HasAttrString(self, name)) {
		RETURN_TRUE;
	} else {
		RETURN_FALSE;
	}
}

/*
 * python_import(name) -> PythonWrapper
 *
 * Throws PythonException on error.
 */
static PHP_FUNCTION(p_import)
{
	char *name;
	int namelen;
	PyObject *module;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s", &name, &namelen) == FAILURE)
		return;

	module = PyImport_ImportModule(name);
	if (module == NULL) {
		p_throw_python_exception(TSRMLS_C);
		return;
	}

	p_make_php_object(return_value, module TSRMLS_CC);
}

static void p_container(int num_args, zval *retval_php, PyObject *(*make)(HashTable *array TSRMLS_DC) TSRMLS_DC)
{
	HashTable *array;
	PyObject *retval_py;

	if (zend_parse_parameters(num_args TSRMLS_CC, "h", &array) == FAILURE)
		return;

	retval_py = make(array TSRMLS_CC);
	if (retval_py == NULL)
		return;

	p_make_php_value(retval_php, retval_py TSRMLS_CC);
}

/*
 * python_tuple(ARRAY) -> PythonWrapper
 *
 * Throws PythonException on error.
 */
static PHP_FUNCTION(p_tuple)
{
	p_container(ZEND_NUM_ARGS(), return_value, p_make_python_tuple TSRMLS_CC);
}

/*
 * python_list(ARRAY) -> PythonWrapper
 *
 * Throws PythonException on error.
 */
static PHP_FUNCTION(p_list)
{
	p_container(ZEND_NUM_ARGS(), return_value, p_make_python_list TSRMLS_CC);
}

/*
 * python_set(ARRAY) -> PythonWrapper
 *
 * Throws PythonException on error.
 */
static PHP_FUNCTION(p_set)
{
	p_container(ZEND_NUM_ARGS(), return_value, p_make_python_set TSRMLS_CC);
}

/*
 * python_frozenset(ARRAY) -> PythonWrapper
 *
 * Throws PythonException on error.
 */
static PHP_FUNCTION(p_frozenset)
{
	p_container(ZEND_NUM_ARGS(), return_value, p_make_python_frozenset TSRMLS_CC);
}

/*
 * python_dict(ARRAY) -> PythonWrapper
 *
 * Throws PythonException on error.
 */
static PHP_FUNCTION(p_dict)
{
	p_container(ZEND_NUM_ARGS(), return_value, p_make_python_dict TSRMLS_CC);
}

/*
 * python_to_array(PythonWrapper) -> ARRAY
 *
 * Throws PythonException on error.
 */
static PHP_FUNCTION(p_to_array)
{
	zval *object_php;
	PyObject *object_py;
	bool mapping;
	PyObject *iter;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "O", &object_php, p_wrapper) == FAILURE)
		return;

	object_py = p_get_python_object(object_php TSRMLS_CC);
	if (object_py == NULL)
		return;

	mapping = PyMapping_Check(object_py);
	if (!mapping && !PySequence_Check(object_py)) {
		p_throw_exception("Python object type not supported" TSRMLS_CC);
		return;
	}

	iter = PyObject_GetIter(object_py);
	if (iter == NULL) {
		p_throw_python_exception(TSRMLS_C);
		return;
	}

	array_init(return_value);

	if (mapping) {
		PyObject *key;
		PyObject *value;
		int ret;

		while ((key = PyIter_Next(iter))) {
			value = PyObject_GetItem(object_py, key);
			if (value) {
				ret = p_make_php_mapping(Z_ARRVAL_P(return_value), key, value TSRMLS_CC);
			} else {
				p_throw_python_exception(TSRMLS_C);
				ret = FAILURE;
			}

			Py_DECREF(key);

			if (ret == FAILURE)
				break;
		}
	} else {
		PyObject *item_py;
		zval *item_php;

		while ((item_py = PyIter_Next(iter))) {
			MAKE_STD_ZVAL(item_php);
			if (p_make_php_value(item_php, item_py) == FAILURE) {
				FREE_ZVAL(item_php);
				break;
			}

			zend_hash_next_index_insert(Z_ARRVAL_P(return_value), &item_php, sizeof (item_php), NULL);
		}
	}

	Py_DECREF(iter);
}

static PHP_MINIT_FUNCTION(p)
{
	static zend_class_entry wrapper;
	static zend_class_entry exception;

	static zend_internal_function wrapper_call = {
		.type    = ZEND_INTERNAL_FUNCTION,
		.handler = ZEND_MN(p_wrapper_call),
	};
	static zend_internal_function wrapper_get = {
		.type    = ZEND_INTERNAL_FUNCTION,
		.handler = ZEND_MN(p_wrapper_get),
	};
	static zend_internal_function wrapper_isset = {
		.type    = ZEND_INTERNAL_FUNCTION,
		.handler = ZEND_MN(p_wrapper_isset),
	};

	Py_InitializeEx(false);

	INIT_OVERLOADED_CLASS_ENTRY_EX(wrapper, P_WRAPPER_NAME, NULL, (zend_function *) &wrapper_call,
	                               (zend_function *) &wrapper_get, NULL, NULL, (zend_function *) &wrapper_isset);
	p_wrapper = zend_register_internal_class(&wrapper TSRMLS_CC);
	if (p_wrapper == NULL)
		return FAILURE;

	zend_declare_property_long(p_wrapper, P_PROPERTY_NAME, strlen(P_PROPERTY_NAME), 0, ZEND_ACC_PRIVATE TSRMLS_CC);

	INIT_CLASS_ENTRY(exception, P_EXCEPTION_NAME, NULL);
	p_exception = zend_register_internal_class_ex(&exception, zend_exception_get_default(TSRMLS_C),
	                                              NULL TSRMLS_CC);
	if (p_exception == NULL)
		return FAILURE;

	p_resource = zend_register_list_destructors_ex(p_destroy_resource, NULL, P_RESOURCE_NAME, module_number);
	if (p_resource == FAILURE)
		return FAILURE;

	return SUCCESS;
}

static PHP_MSHUTDOWN_FUNCTION(p)
{
	Py_Finalize();

	return SUCCESS;
}

static function_entry p_functions[] = {
	PHP_NAMED_FE(python_import,    PHP_FN(p_import),    NULL)
	PHP_NAMED_FE(python_list,      PHP_FN(p_list),      NULL)
	PHP_NAMED_FE(python_set,       PHP_FN(p_set),       NULL)
	PHP_NAMED_FE(python_frozenset, PHP_FN(p_frozenset), NULL)
	PHP_NAMED_FE(python_dict,      PHP_FN(p_dict),      NULL)
	PHP_NAMED_FE(python_to_array,  PHP_FN(p_to_array),  NULL)
	{}
};

static zend_module_entry p_module_entry = {
	STANDARD_MODULE_HEADER,
	"pyhp",
	p_functions,
	PHP_MINIT(p),
	PHP_MSHUTDOWN(p),
	NULL,
	NULL,
	NULL,
	"0.3",
	STANDARD_MODULE_PROPERTIES
};

ZEND_GET_MODULE(p)
